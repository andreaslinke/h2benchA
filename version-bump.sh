#!/bin/sh
# run in top level directory (which has app/build.gradle)
# increment build number in versionCode
# and last part of build version in versionName 
GRADLE_FILE=app/build.gradle

# versionCode 11
awk '/versionCode/{$2 += 1;} {print}' ${GRADLE_FILE} > ${GRADLE_FILE}.tmp && mv ${GRADLE_FILE}.tmp ${GRADLE_FILE}
# versionName "1.0.7"
awk '/versionName/{ gsub(/"/,"",$2);split($2,v,".");$2 = "\"" v[1] "." v[2] "." v[3] + 1 "\"";} {print}' ${GRADLE_FILE} > ${GRADLE_FILE}.tmp && mv ${GRADLE_FILE}.tmp ${GRADLE_FILE}

