package com.linkesoft.h2bencha;

import android.content.Context;
import android.os.Process;
import android.util.Log;

import com.linkesoft.h2bencha.util.BenchmarkDataPoint;
import com.linkesoft.h2bencha.util.BenchmarkResult;
import com.linkesoft.h2bencha.util.Formatter;
import com.linkesoft.h2bencha.util.Prefs;
import com.linkesoft.h2bencha.util.StorageInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Main benchmark class for measuring reading/writing performance to storage
 *
 */
public class Benchmark {
    // various types of benchmarks
    public enum Type {
        SequentialRead,RandomRead,SequentialWrite,RandomWrite,FullWrite;
        public String toString(Context context) {
            switch (this) {
                case SequentialRead:
                    return context.getString(R.string.SequentialRead);
                case RandomRead:
                    return context.getString(R.string.RandomRead);
                case SequentialWrite:
                    return context.getString(R.string.SequentialWrite);
                case RandomWrite:
                    return context.getString(R.string.RandomWrite);
                case FullWrite:
                    return context.getString(R.string.WriteTest);
            }
            return super.toString();
        }
    }

    // progress
    public class Progress {
        public Type type;
        public double percent;
    }

    public interface ProgressUpdateListener {
        void onProgress(Progress progress);
        void benchmarkTypeFinished(Type benchmarkType);
    }

    public ProgressUpdateListener progressUpdateListener=null;


    public final StorageInfo storageInfo;
    protected final Context context;
    protected final int blocksize; // buffer size
    protected final byte[] buffer; // size of blocksize, used for read/write
    private final long mainMemory;
    private final int randomfilesize; // small file size
    private final int sequentialfilesize; // big file size
    private final int numThreads;
    protected boolean isNative;

    private final Random random;
    private final long deltaProgressTimeNano = TimeUnit.NANOSECONDS.convert(1000,TimeUnit.MILLISECONDS); // how often should we update the progress bar/history

    private boolean cancelled = false;
    public volatile boolean stop = false; // set from other threads to stop current benchmark type
    public String lastError = null;

    public Benchmark(Context context, StorageInfo storageInfo, int blocksize, long mainMemory, int numThreads) {
        this.context = context;
        this.storageInfo = storageInfo;
        this.blocksize = blocksize;
        this.buffer = new byte[blocksize];
        this.mainMemory = mainMemory;
        this.numThreads = numThreads;
        this.randomfilesize = Prefs.getRandomFileSize(context);
        this.sequentialfilesize = Prefs.getSequentialFileSize(context);
        this.isNative = false;
        // initialize buffer with random bytes
        random=new Random();
        random.nextBytes(buffer);
    }

    /**
     * Perform all benchmarks on given storage info
     * @return true if success or cancel, false on error
     */
    public boolean measure() {
        storageInfo.benchmarkResults.clear();

        // sequential read/write (big files) up to size of main memory (to avoid any caching)
        long totalSize = Math.min(StorageInfo.getAvailableSize(storageInfo.baseDir) * 9L / 10L / numThreads, mainMemory);
        long fileSize = Math.min(StorageInfo.getAvailableSize(storageInfo.baseDir) * 9L / 10L / numThreads, sequentialfilesize);
        boolean ok = measure(fileSize,totalSize,Type.SequentialRead,Type.SequentialWrite);
        if(!ok)
            return false;
        if(isCancelled())
            return true;
        // random read/write (small files), total size should not be too big otherwise we'll wait forever
        totalSize = Math.min(StorageInfo.getAvailableSize(storageInfo.baseDir) * 9L / 10L / numThreads, 20_000_000L);
        fileSize = randomfilesize;
        ok = measure(fileSize,totalSize,Type.RandomRead,Type.RandomWrite);
        return ok;
    }

    // return true if success or cancel, false on error
    private boolean measure(long fileSize, long totalSize, Type readType, Type writeType) {
        stop=false;
        List<File> files = new ArrayList<>();
        try {
            // write files
            bytesToWork=totalSize;
            bytesWorked=0;
            BenchmarkResult benchmarkResult = new BenchmarkResult(writeType);
            while (totalSize >= fileSize) {
                File file = new File(storageInfo.baseDir, "file-" + Process.myTid() + "-" + files.size());
                files.add(file);
                BenchmarkResult r = new BenchmarkResult(writeType);

                if (!writeFile(file, fileSize, false, r))
                    return false;

                if(isCancelled())
                    return true;

                benchmarkResult.byteCount += r.byteCount;
                benchmarkResult.timeNano += r.timeNano;
                benchmarkResult.history.addAll(r.history);
                benchmarkResult.history.add(new BenchmarkDataPoint(r.timeNano,r.byteCount));
                totalSize -= fileSize;
                bytesWorked += fileSize;
                publishProgress(benchmarkResult.type,0);
                if(stop)
                    break;
            }
            addHistoryDataPoint(benchmarkResult,benchmarkResult.byteCount,benchmarkResult.timeNano);
            benchmarkResult.description=context.getString(R.string.benchmark_result_description,files.size(),Formatter.formatSize(context, fileSize),numThreads,isNative?"C":"Java");
            Log.i(getClass().getSimpleName(),benchmarkResult.info(context).toString()+" ("+Process.myTid()+")");
            storageInfo.benchmarkResults.add(benchmarkResult);
            if(progressUpdateListener!=null)
                progressUpdateListener.benchmarkTypeFinished(writeType);

            // read same files
            stop=false;
            totalSize=bytesWorked;
            bytesToWork=totalSize;
            bytesWorked=0;
            benchmarkResult = new BenchmarkResult(readType);
            for(File file:files) {
                BenchmarkResult r = new BenchmarkResult(readType);

                if(!readFile(file, false, r))
                    return false;

                benchmarkResult.byteCount += r.byteCount;
                benchmarkResult.timeNano += r.timeNano;
                benchmarkResult.history.addAll(r.history);
                benchmarkResult.history.add(new BenchmarkDataPoint(r.timeNano,r.byteCount));
                bytesWorked += r.byteCount;
                publishProgress(benchmarkResult.type,0);
                if(stop)
                    break;
            }
            addHistoryDataPoint(benchmarkResult,benchmarkResult.byteCount,benchmarkResult.timeNano);
            benchmarkResult.description=context.getString(R.string.benchmark_result_description,files.size(),Formatter.formatSize(context, fileSize),numThreads,isNative?"C":"Java");
            Log.i(getClass().getSimpleName(),benchmarkResult.info(context).toString()+" ("+Process.myTid()+")");
            storageInfo.benchmarkResults.add(benchmarkResult);
            if(progressUpdateListener!=null)
                progressUpdateListener.benchmarkTypeFinished(readType);
            stop=false;
            return true;
        } finally {
            for(File file: files) {
                //noinspection ResultOfMethodCallIgnored
                file.delete();
            }
        }
    }

    /**
     * Write as much as we can on storage info and validate on read
     * @return true on success or cancel, false on read/compare error
     */
    public boolean performFulLWriteWithValidation() {
        // reproducible random seed
        long seed = random.nextInt();
        random.setSeed(seed);
        List<File> files=new ArrayList<>();
        bytesToWork = 2 * StorageInfo.getAvailableSize(storageInfo.baseDir); // for write and read
        bytesWorked = 0;
        try {
            while (StorageInfo.getAvailableSize(storageInfo.baseDir) > 0) {
                File file = new File(storageInfo.baseDir, "big" + files.size());
                long fileSize = Math.min(StorageInfo.getAvailableSize(storageInfo.baseDir), sequentialfilesize); // try to write large files for better performance
                BenchmarkResult r = new BenchmarkResult(Type.FullWrite);

                if(!writeFile(file, fileSize, true, r))
                    return false;

                if(isCancelled())
                    return true;
                bytesWorked += fileSize;
                publishProgress(r.type,0);
                files.add(file);
            }

            random.setSeed(seed);
            for(File file: files) {
                BenchmarkResult r = new BenchmarkResult(Type.FullWrite);

                if(!readFile(file,true,r))
                    return false; // error reading/comparing file

                if(isCancelled())
                    return true;
                bytesWorked += file.length();
                publishProgress(r.type,0);
            }
            return true;
        } finally {
            for(File file: files) {
                //noinspection ResultOfMethodCallIgnored
                file.delete();
            }
        }

    }

    private boolean isCancelled() {
        return cancelled;
    }
    public void cancel() {
        cancelled=true;
        Log.d(getClass().getSimpleName(),"Benchmark cancelled");
    }

    private long bytesToWork=0; // per phase
    private long bytesWorked=0;
    private void publishProgress(Benchmark.Type type,long bytesCurrentlyWorked) {
        if(progressUpdateListener!=null) {
            Progress progress = new Progress();
            progress.type = type;
            if(bytesToWork!=0)
                progress.percent = (double)(bytesWorked+bytesCurrentlyWorked)/(double)bytesToWork;
            else
                progress.percent=1;
            progressUpdateListener.onProgress(progress);
        }
    }

    private void addHistoryDataPoint(BenchmarkResult benchmarkResult, long bytesWorked, long deltaTimeNano) {
        benchmarkResult.history.add(new BenchmarkDataPoint(deltaTimeNano,bytesWorked));
    }

    // main function for timing writes
    boolean writeFile(File file, long size, boolean forValidation, BenchmarkResult benchmarkResult) {
        if(benchmarkResult.type != Type.RandomWrite)
            Log.d(getClass().getSimpleName(), "Writing file " + file + " of size " + Formatter.formatSize(context, size));
        long bytesWritten = 0;
        FileOutputStream fileOutputStream = null;

        long startTime = System.nanoTime();
        long lastProgressTimeNano = startTime;
        long lastBytesWritten = 0;
        try {
            fileOutputStream = new FileOutputStream(file);
            // inner time critical loop
            while (bytesWritten < size) {
                if (isCancelled())
                    return true;
                if (forValidation)
                    random.nextBytes(buffer);
                int bytesToWrite = (int) Math.min(size - bytesWritten, buffer.length);
                fileOutputStream.write(buffer, 0, bytesToWrite);
                bytesWritten += bytesToWrite;

                if (System.nanoTime() > lastProgressTimeNano + deltaProgressTimeNano && size - bytesWritten > buffer.length) {
                    // send progress update, and add current result to history
                    publishProgress(benchmarkResult.type, bytesWritten);
                    addHistoryDataPoint(benchmarkResult, bytesWritten - lastBytesWritten, System.nanoTime() - lastProgressTimeNano);
                    lastBytesWritten = bytesWritten;
                    lastProgressTimeNano = System.nanoTime();
                }
                if(stop)
                    break;
            } // inner loop
            fileOutputStream.flush(); // flush stream before final measurement
            benchmarkResult.byteCount = bytesWritten;
            benchmarkResult.timeNano = System.nanoTime() - startTime;
            addHistoryDataPoint(benchmarkResult, benchmarkResult.byteCount, benchmarkResult.timeNano);
            Log.d(getClass().getSimpleName(), "" + Formatter.formatSize(context, bytesWritten) + " bytes written in " + Formatter.formatSeconds(System.nanoTime() - startTime) + ", " + Formatter.formatRate(context, bytesWritten, System.nanoTime() - startTime));
            return true;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Could not write file " + file, e);
            lastError = e.getLocalizedMessage();
            return false;
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }

    // main function for timing reads
    boolean readFile(File file, boolean validate, BenchmarkResult benchmarkResult) {
        if(benchmarkResult.type != Type.RandomRead)
            Log.d(getClass().getSimpleName(), "Reading file " + file + " of size " + Formatter.formatSize(context, file.length()));
        byte[] readbytes = new byte[blocksize];
        long totalBytesRead = 0;
        FileInputStream fileInputStream = null;

        long startTime = System.nanoTime();
        long lastProgressTimeNano = startTime;
        long lastBytesRead = 0;
        try {
            fileInputStream = new FileInputStream(file);
            // inner time-critical loop
            while (true) {
                if (isCancelled())
                    return true;
                if (validate)
                    random.nextBytes(buffer);
                int bytesRead = fileInputStream.read(readbytes);
                if (bytesRead < 0)
                    break; // end of file
                totalBytesRead += bytesRead;
                if (validate) {
                    while (bytesRead < blocksize) {
                        // need to read more bytes
                        bytesRead = fileInputStream.read(readbytes, bytesRead, blocksize - bytesRead);
                        if (bytesRead < 0)
                            break; // end of file
                    }
                    if (bytesRead < 0)
                        break; // end of file
                    if (!Arrays.equals(buffer, readbytes)) {
                        // error, bytes read don't match
                        throw new Exception("ERROR Bytes read do not match for " + file + " at position " + (totalBytesRead - bytesRead));
                    }
                }

                if (System.nanoTime() > lastProgressTimeNano + deltaProgressTimeNano) {
                    // send progress update, and add current result to history
                    publishProgress(benchmarkResult.type, totalBytesRead);
                    addHistoryDataPoint(benchmarkResult, totalBytesRead - lastBytesRead, System.nanoTime() - lastProgressTimeNano);
                    lastBytesRead = totalBytesRead;
                    lastProgressTimeNano = System.nanoTime();
                }
                if(stop)
                    break;
            } // inner loop
            benchmarkResult.byteCount = totalBytesRead;
            benchmarkResult.timeNano = System.nanoTime() - startTime;
            addHistoryDataPoint(benchmarkResult, benchmarkResult.byteCount, benchmarkResult.timeNano);
            if (totalBytesRead != file.length() && !stop) {
                throw new Exception("Error reading file. Read " + totalBytesRead + " expected " + file.length());
            }
            Log.d(getClass().getSimpleName(), Formatter.formatSize(context, totalBytesRead) + " bytes read in " + Formatter.formatSeconds(System.nanoTime() - startTime) + ", " + Formatter.formatRate(context, totalBytesRead, System.nanoTime() - startTime));
            return true;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Could not write file " + file, e);
            lastError = e.getLocalizedMessage();
            return false;
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }
}
