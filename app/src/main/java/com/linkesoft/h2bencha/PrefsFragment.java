package com.linkesoft.h2bencha;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.linkesoft.h2bencha.util.Prefs;
import com.linkesoft.h2bencha.util.SystemInfo;

public class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {



   @Override
   public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       addPreferencesFromResource(R.xml.preferences);
   }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        // disable native preference if app is not compiled with native coding
        if(!BenchmarkNative.hasNativeSupport(getActivity()))
            findPreference(Prefs.NATIVEBENCHMARK).setEnabled(false);
        // update summary for all our preferences
        for(String prefName: sharedPreferences.getAll().keySet()) {
            Preference preference = findPreference(prefName);
            if(preference!=null) {
                if(preference instanceof EditTextPreference)
                    preference.setSummary(sharedPreferences.getString(prefName, ""));
            }
        }
        // register for changes
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
       // sanity check for number of threads
        Preference preference = findPreference(key);
        if(key.equals(Prefs.NUMTHREADS)) {
            int numCores = new SystemInfo(getActivity()).numCores;
            if(Prefs.getNumThreads(getActivity()) > numCores)
                Prefs.setNumThreads(getActivity(),numCores);
            if(Prefs.getNumThreads(getActivity())<=0)
                Prefs.setNumThreads(getActivity(),1);
        }
        // update summary
        if(preference instanceof EditTextPreference)
            preference.setSummary(sharedPreferences.getString(key, ""));
    }
}
