package com.linkesoft.h2bencha;

import android.content.Context;
import android.util.Log;

import com.linkesoft.h2bencha.util.BenchmarkResult;
import com.linkesoft.h2bencha.util.Formatter;
import com.linkesoft.h2bencha.util.StorageInfo;

import java.io.File;

/**
 * Use native C implementation of read/write loop.
 * Enable CMake in build.gradle
 */

public class BenchmarkNative extends Benchmark {
    public native long writeFileJNI(String filepath, long bytesToWrite, byte[] bytes); // return time in ns
    public native long readFileJNI(String filepath, long bytesToRead, byte[] bytes); // return time in ns

    public BenchmarkNative(Context context, StorageInfo storageInfo, int blocksize, long mainMemory) {
        super(context, storageInfo, blocksize, mainMemory,1);
        this.isNative = true;
        System.loadLibrary("h2bench-jni"); // load native library at runtime
    }

    public static boolean hasNativeSupport(Context context) {
        final File nativeLibraryDir = new File(context.getApplicationInfo().nativeLibraryDir);
        final String[] nativeLibs = nativeLibraryDir.list();
        return nativeLibs != null && nativeLibs.length != 0;
    }

    @Override
    boolean writeFile(File file, long size, boolean forValidation, BenchmarkResult benchmarkResult) {
        if(benchmarkResult.type != Type.RandomWrite)
            Log.d(getClass().getSimpleName(), "Native writing file " + file + " of size " + Formatter.formatSize(context, size));
        benchmarkResult.timeNano = writeFileJNI(file.getAbsolutePath(), size, buffer);
        benchmarkResult.byteCount = size;
        Log.d(getClass().getSimpleName(), "Native write " + Formatter.formatSize(context, size) + ": " + Formatter.formatRate(context, benchmarkResult.byteCount, benchmarkResult.timeNano));
        return benchmarkResult.timeNano != 0;
    }

    @Override
    boolean readFile(File file, boolean validate, BenchmarkResult benchmarkResult) {
        if(benchmarkResult.type != Type.RandomRead)
            Log.d(getClass().getSimpleName(), "Native reading file " + file + " of size " + Formatter.formatSize(context, file.length()));
        byte[] readbytes = new byte[blocksize];
        benchmarkResult.timeNano = readFileJNI(file.getAbsolutePath(), file.length(), readbytes);
        benchmarkResult.byteCount = file.length();
        Log.d(getClass().getSimpleName(), "Native read " + Formatter.formatSize(context, file.length()) + ": " + Formatter.formatRate(context, benchmarkResult.byteCount, benchmarkResult.timeNano));
        return benchmarkResult.timeNano != 0;
    }

}
