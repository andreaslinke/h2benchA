package com.linkesoft.h2bencha;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.linkesoft.h2bencha.util.BenchmarkDataPoint;

import java.util.List;

public class ChartActivity extends AppCompatActivity {

    public static final String DATAPOINTS = "DATAPOINTS";
    public static final String TITLE = "TITLE";
    public static final String SUBTITLE = "SUBTITLE";

    private ChartView chartView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        chartView = findViewById(R.id.chartView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //noinspection ConstantConditions
        getSupportActionBar().setTitle(getIntent().getStringExtra(TITLE));
        getSupportActionBar().setSubtitle(getIntent().getStringExtra(SUBTITLE));
        List<BenchmarkDataPoint> dataPoints = getIntent().getParcelableArrayListExtra(DATAPOINTS);
        chartView.dataPoints = dataPoints.toArray(new BenchmarkDataPoint[dataPoints.size()]);
        chartView.notifyDataChanged();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
