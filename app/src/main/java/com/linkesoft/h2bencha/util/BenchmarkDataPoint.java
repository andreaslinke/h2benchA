package com.linkesoft.h2bencha.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;

/**
 * A benchmark data point (timestamp and rate). Parcelable, so we can send it to another activity.
 */
public class BenchmarkDataPoint implements Parcelable {

    public final long timeStampNano;
    public final long deltaTimeNano;
    public final long bytes;
    public final int threadId;

    public BenchmarkDataPoint(long deltaTimeNano,long bytes) {
        timeStampNano=System.nanoTime();
        this.deltaTimeNano=deltaTimeNano;
        this.bytes=bytes;
        this.threadId=Process.myTid();
    }

    public double getRate() {
        return (double)bytes/(double)deltaTimeNano;
    }


// implementation of Parcelable

    protected BenchmarkDataPoint(Parcel in) {
        this.timeStampNano=in.readLong();
        this.deltaTimeNano=in.readLong();
        this.bytes=in.readLong();
        this.threadId=in.readInt();
    }

    public static final Creator<BenchmarkDataPoint> CREATOR = new Creator<BenchmarkDataPoint>() {
        @Override
        public BenchmarkDataPoint createFromParcel(Parcel in) {
            return new BenchmarkDataPoint(in);
        }

        @Override
        public BenchmarkDataPoint[] newArray(int size) {
            return new BenchmarkDataPoint[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(timeStampNano);
        dest.writeLong(deltaTimeNano);
        dest.writeLong(bytes);
        dest.writeInt(threadId);
    }
}
