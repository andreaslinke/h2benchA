package com.linkesoft.h2bencha.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.linkesoft.h2bencha.BuildConfig;
import com.linkesoft.h2bencha.R;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.linkesoft.h2bencha.util.StorageInfo.StorageType.Flash;

/**
 * information about a storage, e.g. Flash or SD card
 */

public class StorageInfo {

    public enum StorageType {
        Flash, // aka "external" + internal memory
        SDCard, // real external
        USBDrive // connected USB drive (path contains usb)
    }

    public final StorageType storageType;
    public final String description;
    public final long totalSize; // in bytes
    public final long availableSize;
    public boolean fullWriteSuccessful;

    public List<BenchmarkResult> benchmarkResults = new ArrayList<>();

    public final File baseDir;

    public static StorageInfo[] storageInfosPreR(Context context) {
        List<StorageInfo> storageInfos=new ArrayList<>();
        File[] dirs= ContextCompat.getExternalFilesDirs(context,null); // does not include USB drives!
        if(dirs.length==0) {
            Log.e("StorageInfo","No externalFilesDirs available");
        }
        for(File dir: dirs) {
            if(dir==null) // can happen if card was just removed
                continue;
            StorageInfo storageInfo;
            if(storageInfos.size() == 0) {
                // first dir is always Flash
                storageInfo=new StorageInfo(dir, Flash, context.getString(R.string.BuiltinFlash));
            } else {
                // try some heuristics to distinguish SD card and USB drive
                // TODO use StorageVolume API
                String basePath = dir.getAbsolutePath().toLowerCase();
                if(basePath.contains("usb") || basePath.contains("media_rw"))
                    storageInfo=new StorageInfo(dir,StorageType.USBDrive, context.getString(R.string.USBStick));
                else if(basePath.contains("extsd"))
                    storageInfo=new StorageInfo(dir,StorageType.SDCard, context.getString(R.string.SDCard));
                else if(storageInfos.size() > 1) // if we already have an SD, it's probably USB
                    storageInfo=new StorageInfo(dir,StorageType.USBDrive, context.getString(R.string.USBStick));
                else // default to SD
                    storageInfo=new StorageInfo(dir,StorageType.SDCard, context.getString(R.string.SDCard));
            }
            storageInfos.add(storageInfo);
        }
        return storageInfos.toArray(new StorageInfo[storageInfos.size()]);
    }
    @RequiresApi(api = Build.VERSION_CODES.R)
    public static StorageInfo[] storageInfos(Context context) {
        List<StorageInfo> storageInfos=new ArrayList<>();
        StorageManager storageManager = (StorageManager)context.getSystemService(Context.STORAGE_SERVICE);
        List<StorageVolume> storageVolumeList=storageManager.getStorageVolumes();
        for(StorageVolume storageVolume: storageVolumeList) {
            if(!storageVolume.getState().equals(Environment.MEDIA_MOUNTED))
                continue;
            StorageInfo storageInfo;
            File dir = storageVolume.getDirectory();
            if(dir == null)
                continue;
            // add app-private folder /Android/data/<bundle>/files
            dir = new File(dir,"/Android/data/"+ BuildConfig.APPLICATION_ID+"/files");
            try {
                dir.mkdirs();
            } catch(SecurityException ex) {
                Log.w("StorageInfo","Could not access "+dir,ex);
                continue;
            }
            String description = storageVolume.getDescription(context);
            if(storageVolume.isPrimary()) {
                storageInfo=new StorageInfo(dir, Flash, description);
            } else {
                // try some heuristics to distinguish SD card and USB drive
                // TODO use StorageVolume API
                String basePath = dir.getAbsolutePath().toLowerCase();
                if(basePath.contains("usb") || basePath.contains("media_rw"))
                    storageInfo=new StorageInfo(dir,StorageType.USBDrive, description);
                else if(basePath.contains("extsd"))
                    storageInfo=new StorageInfo(dir,StorageType.SDCard, description);
                else if(storageInfos.size() > 1) // if we already have an SD, it's probably USB
                    storageInfo=new StorageInfo(dir,StorageType.USBDrive, description);
                else // default to SD
                    storageInfo=new StorageInfo(dir,StorageType.SDCard, description);
            }
            storageInfos.add(storageInfo);
        }
        return storageInfos.toArray(new StorageInfo[storageInfos.size()]);
    }
    public StorageInfo(File baseDir, StorageType storageType, String description) {
        this.baseDir=baseDir;
        this.storageType=storageType;
        availableSize = getAvailableSize(baseDir);
        totalSize = getTotalSize(baseDir);
        /* storage is unified, i.e. above numbers are already internal+external
        if(storageType== Flash) {
            // add up with internal memory
            File internalDir = Environment.getDataDirectory();
            availableSize += getAvailableSize(internalDir);
            totalSize += getTotalSize(internalDir);
        }
        */
        this.description = description;
    }
    public void updateBenchmarkResults(StorageInfo storageInfo) {
        // double loop, but not critical because number is really small
        for (BenchmarkResult benchmarkResultStorageInfo : storageInfo.benchmarkResults) {
            // find benchmark result with correct type
            boolean found = false;
            for (BenchmarkResult benchmarkResult : benchmarkResults) {
                if (benchmarkResult.type.equals(benchmarkResultStorageInfo.type)) {
                    benchmarkResult.addBenchmarkResult(benchmarkResultStorageInfo);
                    found = true;
                }
            }
            if(!found) {
                benchmarkResults.add(benchmarkResultStorageInfo);
            }
        }
    }
    // sort by type
    public void sortBenchmarkResults() {
        Collections.sort(benchmarkResults,new Comparator<BenchmarkResult>() {
            @Override
            public int compare(BenchmarkResult o1, BenchmarkResult o2) {
                return o1.type.compareTo(o2.type);
            }
        });
    }

    public void loadBenchmarkResults(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json=prefs.getString(baseDir.getAbsolutePath(),null);
        if(json!=null) {
            Gson gson = new Gson();
            Type benchmarkResultType = new TypeToken<ArrayList<BenchmarkResult>>(){}.getType();
            benchmarkResults = gson.fromJson(json,benchmarkResultType);
        }
    }

    public void saveBenchmarkResults(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(benchmarkResults);
        edit.putString(baseDir.getAbsolutePath(),json);
        edit.apply();
    }

    public Info info(Context context) {
        String name=description;
        if(fullWriteSuccessful)
            name += " ("+context.getString(R.string.WriteTestSuccesful)+")";
        if(totalSize!=0)
            return new Info(name, context.getString(R.string.available_total_size, Formatter.formatSize(context, availableSize), Formatter.formatSize(context, totalSize)));
        else
            return new Info(name,context.getString(R.string.unavailable));
    }

    public String getRootDir(Context context) {
        // remove /data/<package>/files
        String basePath = baseDir.getAbsolutePath();
        String lastPart = "/data/" + context.getPackageName() + "/files";

        if(basePath.endsWith(lastPart))
            return basePath.substring(0,basePath.length()-lastPart.length());
        else
            return basePath;
    }

    public void cleanup() {
        Log.i(getClass().getSimpleName(),"Cleanup: deleting all files in "+baseDir);
        // delete all files
        for (File file : baseDir.listFiles())
            //noinspection ResultOfMethodCallIgnored
            file.delete();
    }

    public static long getAvailableSize(File dir) {
        if(dir==null)
            return 0; // unavailable
        StatFs stat = new StatFs(dir.getPath());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
        } else {
            //noinspection deprecation
            return (long)stat.getAvailableBlocks() * stat.getBlockSize();
        }
    }
    private static long getTotalSize(File dir) {
        if(dir==null)
            return 0; // unavailable
        StatFs stat = new StatFs(dir.getPath());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return stat.getBlockCountLong() *stat.getBlockSizeLong();
        } else {
            //noinspection deprecation
            return (long)stat.getBlockCount() * stat.getBlockSize();
        }
    }
}
