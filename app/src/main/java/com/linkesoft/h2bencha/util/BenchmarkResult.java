package com.linkesoft.h2bencha.util;

import android.content.Context;

import com.linkesoft.h2bencha.Benchmark;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Result of one type of benchmark (read or write, sequential or random)
 */

public class BenchmarkResult {
    public final Benchmark.Type type;
    public long byteCount;
    public long timeNano;
    public String description;
    public final ArrayList<BenchmarkDataPoint> history = new ArrayList<>();

    public BenchmarkResult(Benchmark.Type type) {
        this.type = type;
    }

    public double rateInMBs() {
        return (double)byteCount/(double) timeNano * (double) TimeUnit.SECONDS.toNanos(1)/(double)Formatter.MEGA;
    }



    public class BenchmarkInfo extends Info {
        public final BenchmarkResult benchmarkResult;

        public BenchmarkInfo(BenchmarkResult benchmarkResult,Context context) {
            super(benchmarkResult.type.toString(context) + ": " + Formatter.formatRate(context, byteCount, timeNano), description);
            this.benchmarkResult = benchmarkResult;
        }
    }

    public void addBenchmarkResult(BenchmarkResult benchmarkResult) {
        // assume benchmark results have (roughly) same time, so just add bytes
        byteCount += benchmarkResult.byteCount;
        history.addAll(benchmarkResult.history); // append history

        // add rates, b1/t1 + b2/t2 = (b1*t2 + b2*t1)/(t1*t2) = (b1 + b2*t1/t2) / t1
        //byteCount += (benchmarkResult.byteCount * timeNano)/benchmarkResult.timeNano;
    }

    public Info info(Context context) {
        return new BenchmarkInfo(this,context);
    }

    public void cleanupHistory() {
        while (history.size() > 1000) {
            // remove every 2nd entry, but keep last
            for(int i=0;i<history.size()-1;i++) {
                history.remove(i);
            }
        }
    }
}
