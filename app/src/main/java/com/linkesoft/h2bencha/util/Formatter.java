package com.linkesoft.h2bencha.util;

import android.content.Context;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

/**
 * Formatting helpers for MB, GHz, MB/s etc.
 */

public class Formatter {

    public static final long KILO = 1024L;
    public static final long MEGA = KILO*KILO;
    public static final long GIGA = KILO*KILO*KILO;

    public static String formatSize(Context context, long bytes) {
        return android.text.format.Formatter.formatShortFileSize(context,bytes);
    }
    /*
    public static String formatSize(long bytes) {
        if(bytes > 500_000_000) {
            double value = bytes/(double)GIGA;
            return new DecimalFormat("#,##0.#").format(value) + " GB";
        } else if(bytes > 500_000) {
            double value = bytes/(double)MEGA;
            return new DecimalFormat("#,##0.#").format(value) + " MB";
        } else {
            double value = (double)bytes;
            return new DecimalFormat("#,##0").format(value) + " B";
        }
    }
    */

    public static String formatGHz(long hz) {
        double value = hz/(double)GIGA;
        return new DecimalFormat("#,##0.##").format(value) + " GHz";
    }

    public static String formatRate(Context context,long bytesCount,long timeNano) {
        if(timeNano==0) {
            return "-";
        }
        double bytesPerSecond = (double)bytesCount * (double)TimeUnit.SECONDS.toNanos(1) / (double)timeNano;
        return android.text.format.Formatter.formatShortFileSize(context, (long)bytesPerSecond)+"/s";
    }

    /*
    public static String formatRate(long bytesCount,long timeNano) {
        if(timeNano==0) {
            return "-";
        }
        double value = (double)bytesCount/(double)timeNano*(double)TimeUnit.SECONDS.toNanos(1)/(double)MEGA;
        return new DecimalFormat("#,##0.#").format(value) + " MB/s";
    }
    */

    public static String formatSeconds(long timeNano) {
        if(timeNano==0) {
            return "-";
        }
        double value = (double)timeNano/(double)TimeUnit.SECONDS.toNanos(1);
        if(value > 0.2)
            return new DecimalFormat("#,##0.#").format(value) + " s";
        value = (double)timeNano/(double)TimeUnit.MILLISECONDS.toNanos(1);
        if(value > 0.2)
            return new DecimalFormat("#,##0.#").format(value) + " ms";
        value = (double)timeNano/(double)TimeUnit.MICROSECONDS.toNanos(1);
        if(value > 0.2)
            return new DecimalFormat("#,##0.#").format(value) + " µs";
        else
            return "" + timeNano + "ns";

    }
}
