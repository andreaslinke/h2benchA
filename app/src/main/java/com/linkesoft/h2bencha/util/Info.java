package com.linkesoft.h2bencha.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple object to stora name or label and value or description
 */

public class Info  {
    public final String name;
    public final String value;
    public Info(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Map<String,String> map() {
        Map<String,String> map = new HashMap<>();
        map.put("NAME",name);
        map.put("VALUE",value);
        return map;
    }
    public static String[] keys() {
        return new String[]{"NAME","VALUE"};
    }
    @Override
    public String toString() {
        return name+": "+value;
    }


}
