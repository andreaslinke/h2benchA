package com.linkesoft.h2bencha.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Helper functions for app settings.
 * Note: default values set in preferences.xml take precedence over defaults set here.
 */

public class Prefs {

    public static final String BLOCKSIZE = "blocksize";
    public static final String RANDOMFILESIZE = "randomfilesize";
    public static final String SEQUENTIALFILESIZE = "sequentialfilesize";
    public static final String NUMTHREADS = "numthreads";
    public static final String NATIVEBENCHMARK = "nativeBenchmark";

    public static int getBlockSize(Context context) {
        return Prefs.getIntWithDefault(context,BLOCKSIZE,8_192);
    }
    public static int getRandomFileSize(Context context) {
        return Prefs.getIntWithDefault(context,RANDOMFILESIZE,8_192);
    }
    public static int getSequentialFileSize(Context context) {
        return Prefs.getIntWithDefault(context,SEQUENTIALFILESIZE,209_715_200);
    }
    public static int getNumThreads(Context context) {
        return Prefs.getIntWithDefault(context,NUMTHREADS,2); // default to 2
    }
    public static void setNumThreads(Context context,int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(NUMTHREADS,Integer.toString(value)).apply();
    }
    public static boolean isNativeBenchmark(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(NATIVEBENCHMARK,true); // default to native
    }
    private static int getIntWithDefault(Context context, String prefsname, int defaultvalue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int value=0;
        try {
            value = Integer.parseInt(prefs.getString(prefsname,""));
        } catch(Error nfe) {
            // ignore parse errors (fallback to default)
        }
        if(value >= 0)
            return value;
        else
            return defaultvalue; // fallback to default
    }
}
