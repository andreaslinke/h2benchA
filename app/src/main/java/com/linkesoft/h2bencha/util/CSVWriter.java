package com.linkesoft.h2bencha.util;

import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;

/**
 * Simple writer for comma-separated values (CSV)
 * locale-aware
 * calls can be chained
 */

public class CSVWriter {

    private final Writer writer;
    private final String quote="\"";
    private final String sep;
    private final String newline="\n";
    private final DecimalFormat format;

    private StringBuffer line=new StringBuffer();

    public CSVWriter(Writer writer) {
        this.writer = writer;
        format = new DecimalFormat("#,##0.##");
        if(format.getDecimalFormatSymbols().getDecimalSeparator()==',')
            sep=";";
        else
            sep=",";
    }

    public CSVWriter beginLine() {
        line=new StringBuffer();
        return this;
    }

    public CSVWriter endLine() throws IOException {
        line.append(newline);
        writer.append(line.toString());
        return this;
    }

    public CSVWriter writeValue(double value) {
        if(line.length()!=0)
            line.append(sep);
        line.append(format.format(value));
        return this;
    }
    public CSVWriter writeValue(String value) {
        if(line.length()!=0)
            line.append(sep);
        value = value.replace(quote,"\\"+quote);
        line.append(quote).append(value).append(quote);
        return this;
    }

    public void close() throws IOException {
        writer.close();
    }
}
