package com.linkesoft.h2bencha.util;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * General info on Android device, like brand, model, memory, cpu
 * Created by andreas on 27/11/2016.
 */

public class SystemInfo {
    public final long totalMemory;
    public final long processorFrequencyKHz;
    public final int numCores;
    public final String deviceName;
    private final Context context;

    public SystemInfo(Context context) {
        this.context = context;
        ActivityManager actManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            totalMemory = memInfo.totalMem;  // total RAM minus some stuff used by kernel
        }
        else {
            // TODO read /proc/meminfo on 4.0
            totalMemory = 500_000_000L;
        }
        processorFrequencyKHz = readMaxFrequency();
        numCores = Runtime.getRuntime().availableProcessors();
        String deviceName = Build.BRAND+" "+Build.MODEL;
        // Build.BRAND is usually lowercase
        if(deviceName.length()>0 && !Character.isUpperCase(deviceName.charAt(0))) {
            deviceName = Character.toUpperCase(deviceName.charAt(0))+deviceName.substring(1);
        }
        this.deviceName=deviceName;
    }
    
    public String info() {
        final StringBuilder info = new StringBuilder();
        info.append(deviceName).append(" Android ").append(Build.VERSION.RELEASE);

        if (totalMemory!=0) {
            info.append("\nRAM ").append(Formatter.formatSize(context,totalMemory));
        }
        if(processorFrequencyKHz!=0)
            info.append("\nProcessor ").append(Formatter.formatGHz(processorFrequencyKHz*Formatter.KILO)).append(" Cores: ").append(numCores);
        return info.toString();
    }

    // in kHz
    private long readMaxFrequency() {
        String strMaxFrequency=readFile("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
        if(strMaxFrequency.equals(""))
            return 0; // cpu info not found
        try {
            return Long.parseLong(strMaxFrequency);
        } catch(NumberFormatException e) {
            Log.e(getClass().getSimpleName(),"Invalid cpuinfo_max_freq "+strMaxFrequency,e);
            return 0;
        }
    }

    /**
     * read complete file into memory
     * @param path file path to read
     * @return file contents as string, or "" on failure
     */
    private String readFile(String path) {
        File file=new File(path);
        try {
            final InputStream inputStream = new FileInputStream(file);
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            final StringBuilder stringBuilder = new StringBuilder();

            boolean done = false;

            while (!done) {
                final String line = reader.readLine();
                done = (line == null);

                if (line != null) {
                    stringBuilder.append(line);
                }
            }
            reader.close();
            inputStream.close();

            return stringBuilder.toString();
        }
        catch (FileNotFoundException e) {
            Log.e(getClass().getSimpleName(), "File not found "+path+": " + e.toString());
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(), "Cannot read file " +path+": " + e.toString());
        }

        return "";

    }
}
