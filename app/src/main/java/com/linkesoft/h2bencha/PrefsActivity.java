package com.linkesoft.h2bencha;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;

/**
 * Preferences for app
 */

public class PrefsActivity extends AppCompatActivity {
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();
    }

    private void reset() {
        // delete all preferences, so they're reset to default values
        // note: this will also delete our stored benchmark results
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().clear().apply();
        // and set default values from XML (this will not overwrite existing values, so we have to clear them first)
        PreferenceManager.setDefaultValues(this,R.xml.preferences,true);
        // restart activity to update UI
        finish();
        startActivity(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_prefs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset) {
            reset();
            return true;
        } else if (id == R.id.action_send_log) {
            sendLog();
            return true;
        }
        return false;
    }

    private void sendLog() {
        try {
            File logfile=new File(Environment.getExternalStorageDirectory(),
                    "h2benchA-"+getAppVersion()+".txt");
            Runtime.getRuntime().exec(
                    "logcat -d -f " + logfile.getAbsolutePath());
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(logfile));
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, "h2benchA Log");
            sendIntent.putExtra(Intent.EXTRA_TITLE, logfile.getName());
            startActivity(Intent.createChooser(sendIntent , getString(R.string.action_send_log)));
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),"Can't send log",e);
        }

    }

    private String getAppVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(),PackageManager.GET_META_DATA);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(), "package error", e); //$NON-NLS-1$
            return "1.0";
        }
    }
}
