package com.linkesoft.h2bencha;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Process;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.linkesoft.h2bencha.util.Prefs;
import com.linkesoft.h2bencha.util.StorageInfo;
import com.linkesoft.h2bencha.util.SystemInfo;

/**
 * Background task to perform benchmarks, shows progress dialog
 */

public class BenchmarkTask extends AsyncTask<StorageInfo[], Benchmark.Progress, Void> {

    public interface StorageInfoUpdateListener {
        void storageInfoUpdated(StorageInfo storageInfo);
    }
    public interface BenchmarkTaskUpdateListener {
        void benchmarkTypeFinished(Benchmark.Type type); // not on UI thread
        void finished(); // on UI thread
    }

    private final Activity activity;
    private final SystemInfo systemInfo;
    private final boolean fullwrite;
    private Benchmark currentBenchmark;
    private final int numThreads;

    public ProgressDialog progressDialog;
    public StorageInfoUpdateListener storageInfoUpdateListener;
    public BenchmarkTaskUpdateListener benchmarkTaskUpdateListener;

    public BenchmarkTask(Activity activity, boolean fullwrite, int numThreads) {

        this.activity = activity;
        this.systemInfo = new SystemInfo(activity);
        this.fullwrite = fullwrite;
        this.numThreads = numThreads;
    }

    public void cancel() {
        cancel(false);
        if(currentBenchmark!=null)
            currentBenchmark.cancel();
        if(progressDialog!=null)
            progressDialog.dismiss();
    }
    public boolean isStarted() {
        return currentBenchmark != null;
    }
    public boolean isStopped() {
        return currentBenchmark.stop;
    }
    public void stop() {
        Log.i(getClass().getSimpleName(),"Stopping current benchmark type");
        if(currentBenchmark!=null)
            currentBenchmark.stop=true;
    }

    public void setupProgressDialog(ProgressDialog progressDialog) {
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(10_000);
        progressDialog.setProgress(0);
        progressDialog.setTitle(R.string.measuring);
        progressDialog.setMessage(""); // needs to be set here so we can update it later
        progressDialog.setProgressNumberFormat(null); // do not show numbers at right
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel();
            }
        });
        this.progressDialog = progressDialog;
    }

    @Override
    protected Void doInBackground(StorageInfo[]... params) {
        // get block size
        int blocksize = Prefs.getBlockSize(activity);
        for (final StorageInfo storageInfo : params[0]) {

            if(Prefs.isNativeBenchmark(activity))
                currentBenchmark = new BenchmarkNative(activity,storageInfo,blocksize,systemInfo.totalMemory);
            else
                currentBenchmark = new Benchmark(activity,storageInfo,blocksize,systemInfo.totalMemory,numThreads);
            currentBenchmark.progressUpdateListener = new Benchmark.ProgressUpdateListener() {
                @Override
                public void onProgress(Benchmark.Progress progress) {
                    publishProgress(progress);
                }

                @Override
                public void benchmarkTypeFinished(final Benchmark.Type benchmarkType) {
                    if (benchmarkTaskUpdateListener != null) {
                        benchmarkTaskUpdateListener.benchmarkTypeFinished(benchmarkType);
                    }
                }
            };
            if (fullwrite) {
                // full write test only for SD Card
                if (storageInfo.storageType == StorageInfo.StorageType.Flash)
                    continue;
                storageInfo.fullWriteSuccessful = currentBenchmark.performFulLWriteWithValidation();
                if (storageInfo.fullWriteSuccessful)
                    showMessage(activity.getString(R.string.WriteTestSuccesful));
                else
                    showMessage(currentBenchmark.lastError);
            } else {
                //if (storageInfo.storageType != StorageInfo.StorageType.SDCard)
                //    testWithDifferentBlocksizes(storageInfo);
                Log.i(getClass().getSimpleName(),"Starting benchmark thread " + Process.myTid() + " ("+numThreads+" threads)");
                if (!currentBenchmark.measure())
                    showMessage(currentBenchmark.lastError);
                if(isCancelled())
                    Log.i(getClass().getSimpleName(),"Cancelled benchmark thread "+ Process.myTid());
                else
                    Log.i(getClass().getSimpleName(),"Finished benchmark thread "+ Process.myTid());
            }
            if (storageInfoUpdateListener != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        storageInfoUpdateListener.storageInfoUpdated(storageInfo);
                    }
                });
            }
            if (isCancelled())
                break;
        } // for storageInfo
        return null;
    }

    private void testWithDifferentBlocksizes(StorageInfo storageInfo) {
        int blocksizes[]={512, 4_096,8_192,16_000,256_000};
        for(int b: blocksizes) {
            Log.i(getClass().getSimpleName(),"Block size "+b);
            currentBenchmark = new Benchmark(activity,storageInfo,b,systemInfo.totalMemory,numThreads);
            currentBenchmark.measure();
        }
    }

    private void showMessage(final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View fabView = activity.getWindow().getDecorView().getRootView().findViewById(R.id.fab);
                Snackbar.make(fabView, message != null ? message : activity.getString(R.string.Error), Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        })
                        .show();
            }
        });
    }



    @Override
    protected void onProgressUpdate(Benchmark.Progress... values) {
        super.onProgressUpdate(values);
        if(progressDialog!=null) {
            Benchmark.Progress progress = values[0];
            String description = numThreads+" T/" + (currentBenchmark.isNative ? "C" : "Java");
            progressDialog.setMessage(progress.type.toString(activity) +" " + description + "\n" + currentBenchmark.storageInfo.getRootDir(activity));
            int progressvalue = (int) (progress.percent * 10_000.0);
            progressDialog.setProgress(progressvalue);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(benchmarkTaskUpdateListener!=null)
            benchmarkTaskUpdateListener.finished();
        if(currentBenchmark!=null)
            currentBenchmark.storageInfo.cleanup();

    }


}
