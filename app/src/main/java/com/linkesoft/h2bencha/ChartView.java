package com.linkesoft.h2bencha;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.linkesoft.h2bencha.util.BenchmarkDataPoint;
import com.linkesoft.h2bencha.util.Formatter;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Simple chart display of benchmark results (rate vs time)
 */

public class ChartView extends View {

    // all data points, sorted by threadId, we start a new graph when we find a new threadId
    public BenchmarkDataPoint[] dataPoints = new BenchmarkDataPoint[0];

    private long minX = 0;
    private long maxX = 0;
    private double minY = 0;
    private double maxY = 0;

    // https://material.io/guidelines/style/color.html#color-color-palette
    final String foregroundColors[]={"#FF9800", "#CDDC39", "#009688", "#2196F3", "#9C27B0", "#795548", "#FFC107", "#8BC34A", "#00BCD4", "#3F51B5", "#E91E63", "#FF5722", "#FFEB3B", "#4CAF50", "#03A9F4", "#673AB7", "#F44336"};
    final int backgroundColor;
    final int axisColor;
    final DashPathEffect dashPathEffect = new DashPathEffect(new float[]{10, 20}, 0); // does not work with hardware accelerated views, see manifest android:hardwareAccelerated="false"
    final int textSize;

    private int markerIndex = -1;

    @SuppressWarnings("deprecation")
    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //foregroundColor = context.getResources().getColor(R.color.colorAccent);
        backgroundColor = context.getResources().getColor(R.color.chartBackgroundColor);
        axisColor = context.getResources().getColor(R.color.chartAxisColor);

        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.textAppearanceLarge, typedValue, true);
        int[] attribute = new int[]{android.R.attr.textSize};
        TypedArray array = context.obtainStyledAttributes(typedValue.resourceId, attribute);
        textSize = array.getDimensionPixelSize(0, -1);
        array.recycle();
    }

    private float x2point(long x) {
        if (minX < maxX) {
            return (float) (x - minX) / (float) (maxX - minX) * getWidth();
        } else {
            return 0;
        }
    }

    private int point2x(float x) {
        return (int) (x / getWidth() * (maxX - minX) + minX);
    }

    private float y2point(double y) {
        // upside down: lowest value is at highest ypoint
        if (maxY > 0) {
            return getHeight() - (float) (y / maxY) * getHeight();
        } else {
            return getHeight();
        }
    }

    private float point2y(float y) {
        y = getHeight() - y; // upside down
        return (float) (y / getHeight() * maxY);
    }

    public void notifyDataChanged() {
        // calc min/max
        minX = maxX = 0;
        minY = maxY = 0;
        for (BenchmarkDataPoint dataPoint : dataPoints) {
            if (minX == 0 || minX > dataPoint.timeStampNano)
                minX = dataPoint.timeStampNano;
            if (maxX == 0 || maxX < dataPoint.timeStampNano)
                maxX = dataPoint.timeStampNano;
            if (minY == 0 || minY > dataPoint.getRate())
                minY = dataPoint.getRate();
            if (maxY == 0 || maxY < dataPoint.getRate())
                maxY = dataPoint.getRate();
        }
        invalidate();
    }

    private void drawHorizontalAxis(Canvas canvas) {
        BenchmarkDataPoint dataPoint = dataPointForHorizontalAxis();
        if (dataPoint!=null) {
            Paint p = new Paint();
            p.setColor(axisColor);
            p.setStrokeWidth(2);
            p.setStyle(Paint.Style.STROKE);
            p.setPathEffect(dashPathEffect);
            float yAxis = y2point(dataPoint.getRate());
            canvas.drawLine(0, yAxis, getWidth(), yAxis, p);
        }
    }
    private void drawHorizontalAxisLabel(Canvas canvas) {
        // draw value label of axis formatted
        BenchmarkDataPoint dataPoint = dataPointForHorizontalAxis();
        if (dataPoint!=null) {
            Paint p = new Paint();
            p.setColor(axisColor);
            float yAxis = y2point(dataPoint.getRate());
            p.setStyle(Paint.Style.FILL); // for text
            p.setTextSize(textSize);
            canvas.drawText(Formatter.formatRate(getContext(), dataPoint.bytes, dataPoint.deltaTimeNano), 0, yAxis - 5, p);
        }
    }

    private BenchmarkDataPoint dataPointForHorizontalAxis() {
        if(dataPoints.length == 0)
            return null;
        // last data point for first thread
        int threadId = dataPoints[0].threadId;
        for(int i=0;i<dataPoints.length;i++) {
            BenchmarkDataPoint dataPoint = dataPoints[i];
            if(dataPoint.threadId!=threadId)
                return dataPoints[i-1];
        }
        // only on thread, return last point
        return dataPoints[dataPoints.length - 1];
    }

    private void drawGraph(Canvas canvas) {
        // actual data points
        Paint p = new Paint();
        int colorind = 0;
        p.setColor(Color.parseColor(foregroundColors[colorind]));
        p.setStrokeWidth(3);
        p.setStyle(Paint.Style.STROKE);
        if (dataPoints.length > 1) {
            float x = x2point(dataPoints[0].timeStampNano);
            float y = y2point(dataPoints[0].getRate());
            int threadId = dataPoints[0].threadId;
            for (int i = 1; i < dataPoints.length; i++) {
                if(threadId!=dataPoints[i].threadId) {
                    // start a new graph
                    threadId = dataPoints[i].threadId;
                    x = x2point(dataPoints[i].timeStampNano);
                    y = y2point(dataPoints[i].getRate());
                    // new color
                    colorind = (colorind+1) % foregroundColors.length;
                    p.setColor(Color.parseColor(foregroundColors[colorind]));
                    continue;
                }
                float xx = x2point(dataPoints[i].timeStampNano);
                float yy = y2point(dataPoints[i].getRate());
                canvas.drawLine(x, y, xx, yy, p);
                x = xx;
                y = yy;
            }
        }
    }

    private void drawMarker(Canvas canvas, int markerIndex) {
        BenchmarkDataPoint dataPoint = dataPoints[markerIndex];
        Paint pmark = new Paint();
        pmark.setColor(axisColor);
        pmark.setTextSize(textSize);
        final int r = textSize/5;
        String labelX = Formatter.formatSeconds(dataPoint.timeStampNano - dataPoints[0].timeStampNano);
        String labelY = Formatter.formatRate(getContext(), dataPoint.bytes, dataPoint.deltaTimeNano);
        Rect boundsLabelX = new Rect();
        Rect boundsLabelY = new Rect();
        pmark.getTextBounds(labelX, 0, labelX.length(), boundsLabelX);
        pmark.getTextBounds(labelY, 0, labelY.length(), boundsLabelY);
        // draw it left/right, above/below current point
        Rect bounds = new Rect(
                0, 0,
                Math.max(boundsLabelX.width(), boundsLabelY.width()),
                (boundsLabelX.height() + boundsLabelY.height() + r)
        );
        final float x = x2point(dataPoint.timeStampNano);
        final float y = y2point(dataPoint.getRate());
        bounds.offset((int) (x + 2 * r), (int) (y + 2 * r) - bounds.height() - 2 * r);
        if (bounds.right > getWidth())
            bounds.offset(-bounds.width() - 2 * r, 0);
        if (bounds.top < 0)
            bounds.offset(0, bounds.height() + 2 * r);
        Paint pback = new Paint(pmark);
        pback.setColor(backgroundColor);
        pback.setAlpha(128);
        RectF rback = new RectF(bounds);
        rback.inset(-2, -2);
        // draw background dimmed
        canvas.drawRoundRect(rback, 2, 2, pback);
        // draw circle over background
        canvas.drawCircle(x, y, r, pmark);
        // draw text over background
        canvas.drawText(labelY, bounds.left, bounds.top + boundsLabelY.height(), pmark);
        canvas.drawText(labelX, bounds.left, bounds.top + boundsLabelY.height() + r + boundsLabelX.height(), pmark);
    }
    private Timer markerTimer=null;
    private void scheduleMarkerOff() {
        if(markerTimer!=null)
            markerTimer.cancel();
        markerTimer=new Timer();
        markerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                markerIndex=-1;
                markerTimer=null;
                // must invalidate on UI thread
                ChartView.this.post(new Runnable() {
                    @Override
                    public void run() {
                        invalidate();
                    }
                });
            }
        },5_000);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.v(getClass().getSimpleName(), "touch " + event);         //$NON-NLS-1$
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            return true;
        if (event.getAction() == MotionEvent.ACTION_UP) {
            markerIndex = getNearestChartItemIndex(event.getX(), event.getY());
            invalidate();
            scheduleMarkerOff();
            return true;
        }
        return super.onTouchEvent(event);
    }

    private int getNearestChartItemIndex(float x, float y) {
        // min square distance, brute force over all items because there can be multiple graphs
        float minSquareDistance = Float.MAX_VALUE;
        int ind = 0;
        for (int i = 0; i < dataPoints.length; i++) {
            float xx = x2point(dataPoints[i].timeStampNano);
            float yy = y2point(dataPoints[i].getRate());
            float squareDistance = (x - xx) * (x - xx) + (y - yy) * (y - yy);
            if (squareDistance < minSquareDistance) {
                ind = i;
                minSquareDistance = squareDistance;
            }
        }
        return ind;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawHorizontalAxis(canvas);
        drawGraph(canvas);
        drawHorizontalAxisLabel(canvas);
        if (markerIndex >= 0 && markerIndex < dataPoints.length) {
            drawMarker(canvas, markerIndex);
        }
    }
}
