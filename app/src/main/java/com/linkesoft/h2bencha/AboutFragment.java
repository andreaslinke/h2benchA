package com.linkesoft.h2bencha;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

/**
 * Show version/about info
 */
public class AboutFragment extends DialogFragment {

    public AboutFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dlg = super.onCreateDialog(savedInstanceState);
        dlg.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dlg;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        TextView versionView = view.findViewById(R.id.version);
        versionView.setText(getAppVersion());

        // clickable link
        TextView ctlinkView= view.findViewById(R.id.ctlink);
        ctlinkView.setMovementMethod(LinkMovementMethod.getInstance()); // make tapable
        // open custom link
        ClickableSpan clickable = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                String url="http://www.ct.de";
                widget.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        };
        SpannableString f = new SpannableString(ctlinkView.getText()) ;
        f.setSpan(clickable,  0, f.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ctlinkView.setText(f);
        return view;
    }

    private String getAppVersion() {
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getContext().getPackageName(),
                    PackageManager.GET_META_DATA);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(), "package error", e); //$NON-NLS-1$
            return "1.0";
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


}
