package com.linkesoft.h2bencha;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.linkesoft.h2bencha.util.BenchmarkResult;
import com.linkesoft.h2bencha.util.CSVWriter;
import com.linkesoft.h2bencha.util.Formatter;
import com.linkesoft.h2bencha.util.Info;
import com.linkesoft.h2bencha.util.Prefs;
import com.linkesoft.h2bencha.util.StorageInfo;
import com.linkesoft.h2bencha.util.SystemInfo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.linkesoft.h2bencha.R.id.listView;

public class MainActivity extends AppCompatActivity {
    private SystemInfo systemInfo; // general info about system, like memory and processor
    private StorageInfo[] storageInfos; // info about various storage systems (Flash, SD card)

    private TextView infoTextView;
    private ExpandableListView storageListView;
    private static Parcelable storageListState; // to remember list state when switching activities
    private final List<BenchmarkTask> currentBenchmarkTasks = new ArrayList<>(); // list of active benchmark tasks (multiple threads)

    private final int requestAllAccess = 2;
    private final BroadcastReceiver mediaChangeBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(getClass().getSimpleName(), "media broadcast: " + action);
            restart();
            /*
                refresh();
                // refresh again after short delay because sometimes Android needs until it correctly reports the available storage
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh(); // refresh to get current storage list
                    }
                }, 1000);
                */
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // set default pref values from XML (will not overwrite changed values)
        PreferenceManager.setDefaultValues(this,R.xml.preferences,true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                measure(storageInfos);
            }
        });
        systemInfo = new SystemInfo(this);
        infoTextView=findViewById(R.id.infoTextView);
        storageListView=findViewById(listView);

        // open history for result items
        storageListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                StorageInfo storageInfo = storageInfos[groupPosition];
                BenchmarkResult result = storageInfo.benchmarkResults.get(childPosition);
                if(result.history.size()>1) {
                    // make sure history is not too large
                    result.cleanupHistory();
                    Intent i = new Intent(MainActivity.this, ChartActivity.class);
                    i.putParcelableArrayListExtra(ChartActivity.DATAPOINTS, result.history);
                    i.putExtra(ChartActivity.TITLE, result.type.toString(MainActivity.this));
                    i.putExtra(ChartActivity.SUBTITLE,storageInfo.info(MainActivity.this).name);
                    startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });

        // single measurement on long click of storage item
        registerForContextMenu(storageListView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if(!Environment.isExternalStorageManager()) {
                // app setting to access all storage
                Intent i = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                startActivityForResult(i,requestAllAccess);
            }
        }
    }

    private void refresh() {
        Log.d(getClass().getSimpleName(),"refresh");
        infoTextView.setText(systemInfo.info());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // 30 aka Android 11
            storageInfos = StorageInfo.storageInfos(this);
        } else {
            storageInfos = StorageInfo.storageInfosPreR(this);
        }
        List<Map<String,String>> groupData = new ArrayList<>();
        List<List<Map<String,String>>> childData = new ArrayList<>();
        for(StorageInfo storageInfo:storageInfos) {
            storageInfo.loadBenchmarkResults(this);
            groupData.add(storageInfo.info(this).map());
            List<Map<String,String>> childList = new ArrayList<>();
            storageInfo.sortBenchmarkResults();
            for(BenchmarkResult result:storageInfo.benchmarkResults) {
                childList.add(result.info(this).map());
            }
            childData.add(childList);
        }

        ExpandableListAdapter listAdapter = new SimpleExpandableListAdapter(this,
                groupData,android.R.layout.simple_expandable_list_item_2, Info.keys(),new int[] { android.R.id.text1,android.R.id.text2 },
                childData,android.R.layout.simple_expandable_list_item_2, Info.keys(),new int[] { android.R.id.text1,android.R.id.text2}) {
                                        // set background color for child items
                                       @Override
                                       public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
                                           View view = super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);
                                           //noinspection deprecation
                                           view.setBackgroundColor(getResources().getColor(R.color.childListBackgroundColor));
                                           return view;
                                       }
                                   };
        storageListView.setAdapter(listAdapter);
        // expand all by default
        for(int i=0;i<listAdapter.getGroupCount();i++)
            storageListView.expandGroup(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == requestAllAccess) {
            refresh();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    // restart activity in case of media mount/unmount changes (a mere refresh is not enough because the activity seems to cache the storage paths)
    private void restart() {
        Log.i(getClass().getSimpleName(),"media change: restarting activity");
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        Runtime.getRuntime().exit(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
        // restore list state if possible
        storageListView.post(new Runnable() {
            @Override
            public void run() {
                if(storageListState!=null)
                    storageListView.onRestoreInstanceState(storageListState);
                storageListState=null;
            }
        });
        // register for storage changes (e.g. SD card mounted/unmounted)
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addDataScheme("file"); // ?
        for(StorageInfo storageInfo: storageInfos) {
            storageInfo.cleanup(); // delete any spurious files left from last benchmark
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            StorageManager storageManager = (StorageManager) getSystemService(Context.STORAGE_SERVICE);
            storageManager.registerStorageVolumeCallback(getMainExecutor(), new StorageManager.StorageVolumeCallback() {
                  @Override
                  public void onStateChanged(@NonNull StorageVolume volume) {
                      Log.v(getClass().getSimpleName(),"Storage state changed");
                      refresh();
                  }
              }
            );
        } else {
            registerReceiver(mediaChangeBroadcastReceiver, filter);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        storageListState=storageListView.onSaveInstanceState();
        if(mediaChangeBroadcastReceiver!=null)
            unregisterReceiver(mediaChangeBroadcastReceiver);
        // do not let benchmark continue if app is put in background
        for(BenchmarkTask currentBenchmarkTask: currentBenchmarkTasks) {
            currentBenchmarkTask.cancel();
        }
        currentBenchmarkTasks.clear();
    }


    private void measure(final StorageInfo storageInfos[]) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        for(StorageInfo storageInfo: storageInfos)
            storageInfo.cleanup();
        checkAvailableSpace(storageInfos);
        System.gc(); // force garbage collection before we start measuring
        int numThreads = Prefs.getNumThreads(this);
        for(int i=0;i<numThreads;i++) {
            // clone storage infos so each thread can update their own results
            List<StorageInfo> storageInfosForTask = new ArrayList<>();
            for(StorageInfo storageInfo: storageInfos) {
                StorageInfo storageInfoForTask = new StorageInfo(storageInfo.baseDir,storageInfo.storageType, storageInfo.description);
                storageInfosForTask.add(storageInfoForTask);
                // reset results
                storageInfo.benchmarkResults.clear();
            }
            // create benchmark task for this thread
            final BenchmarkTask currentBenchmarkTask = new BenchmarkTask(this, false, numThreads);
            if(i==0) {
                // only one thread shows progress dialog
                ProgressDialog progressDialog = new ProgressDialog(this);
                currentBenchmarkTask.setupProgressDialog(progressDialog);
                progressDialog.show();
            }
            currentBenchmarkTask.storageInfoUpdateListener = new BenchmarkTask.StorageInfoUpdateListener() {
                @Override
                public void storageInfoUpdated(StorageInfo storageInfo) {
                    // sum up storage info benchmark results
                    for(StorageInfo storageInfoActivity: storageInfos) {
                        if(storageInfoActivity.baseDir.equals(storageInfo.baseDir)) {
                            storageInfoActivity.updateBenchmarkResults(storageInfo);
                            storageInfoActivity.saveBenchmarkResults(getApplicationContext());
                            break;
                        }
                    }
                    refresh();
                }
            };
            currentBenchmarkTask.benchmarkTaskUpdateListener = new BenchmarkTask.BenchmarkTaskUpdateListener() {
                @Override
                public void benchmarkTypeFinished(Benchmark.Type type) {
                    // not on main thread
                    if(!currentBenchmarkTask.isStopped()) {
                        // stop all others
                        for(BenchmarkTask benchmarkTask: currentBenchmarkTasks) {
                            if(benchmarkTask != currentBenchmarkTask) {
                                if(!benchmarkTask.isStarted()) {
                                    // task was not started yet -> too many threads
                                    Log.e(getClass().getSimpleName(),"Task should be stopped but was not yet started -> too many threads");
                                    for(BenchmarkTask task: currentBenchmarkTasks) {
                                        task.cancel();
                                    }
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            showError(getString(R.string.tooManyThreads));
                                        }
                                    });
                                    break;
                                } else {
                                    benchmarkTask.stop();
                                }
                            }
                        }
                    }
                }

                @Override
                public void finished() {
                    if(currentBenchmarkTask.progressDialog!=null)
                        currentBenchmarkTask.progressDialog.dismiss();
                    currentBenchmarkTasks.remove(currentBenchmarkTask);
                    if(currentBenchmarkTasks.size() == 0) {
                        // finished
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    }
                }
            };
            currentBenchmarkTasks.add(currentBenchmarkTask);
            // run tasks in parallel, note: execute() would just serialize them
            // @see https://developer.android.com/reference/android/os/AsyncTask.html#execute(Params...)
            // According to the Android sources, AsyncTask.THREAD_POOL_EXECUTOR ony configures up to 4 threads ,
            // so we use a custom executor with as many threads as we need
            Executor threadPoolExecutor = new ScheduledThreadPoolExecutor(numThreads);
            currentBenchmarkTask.executeOnExecutor(threadPoolExecutor,storageInfosForTask.toArray(new StorageInfo[storageInfosForTask.size()]));
        }
    }

    private void showError(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void fullwrite(StorageInfo storageInfos[]) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        for(StorageInfo storageInfo: storageInfos)
            storageInfo.cleanup();
        final BenchmarkTask currentBenchmarkTask = new BenchmarkTask(this,true,1); // only 1 thread
        final ProgressDialog progressDialog = new ProgressDialog(this);
        currentBenchmarkTask.setupProgressDialog(progressDialog);
        progressDialog.show();
        currentBenchmarkTask.storageInfoUpdateListener=new BenchmarkTask.StorageInfoUpdateListener() {
            @Override
            public void storageInfoUpdated(StorageInfo storageInfo) {
                storageInfo.saveBenchmarkResults(getApplicationContext());
                refresh();
            }
        };
        currentBenchmarkTask.benchmarkTaskUpdateListener = new BenchmarkTask.BenchmarkTaskUpdateListener() {
            @Override
            public void benchmarkTypeFinished(Benchmark.Type type) {
                // ignore
            }

            @Override
            public void finished() {
                progressDialog.dismiss();
                currentBenchmarkTasks.remove(currentBenchmarkTask);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        };
        currentBenchmarkTasks.add(currentBenchmarkTask);
        currentBenchmarkTask.execute(storageInfos);
    }

    void checkAvailableSpace(StorageInfo[] storageInfos) {
        // check if we have enough space to write, otherwise measurements will be unreliable
        for(StorageInfo storageInfo: storageInfos) {
            if(storageInfo.availableSize < systemInfo.totalMemory / 2) {
                String warning = getString(R.string.lowAvailableSize,
                        storageInfo.storageType,
                        Formatter.formatSize(this,storageInfo.availableSize),
                        Formatter.formatSize(this,systemInfo.totalMemory));
                Snackbar.make(findViewById(R.id.fab), warning, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        })
                        .show();
            }
        }
    }

    // create file with results as CSV
    private File csvResults() {
        File file = new File(getExternalCacheDir(),systemInfo.deviceName+".csv");
        file.deleteOnExit();
        Log.d(getClass().getSimpleName(),"Writing CSV to "+file);
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter(file));
            // header
            csvWriter.beginLine();
            csvWriter.writeValue(getString(R.string.Model));
            csvWriter.writeValue(getString(R.string.StorageType));
            // assume all results have same benchmark types and are sorted already (in refresh)
            if(storageInfos.length>0) {
                StorageInfo storageInfo = storageInfos[0];
                for(BenchmarkResult result:storageInfo.benchmarkResults) {
                    csvWriter.writeValue(result.type.toString(this)+" (MB/s)");
                }
                }
            csvWriter.endLine();
            // data
            for(StorageInfo storageInfo: storageInfos) {
                csvWriter.beginLine();
                csvWriter.writeValue(systemInfo.deviceName);
                csvWriter.writeValue(storageInfo.storageType.toString());
                for(BenchmarkResult result:storageInfo.benchmarkResults) {
                    csvWriter.writeValue(result.rateInMBs());
                }
                csvWriter.endLine();
            }
            csvWriter.close();
            return file;
        } catch (IOException e) {
            Log.e(getClass().getSimpleName(),"Could not create CSV file with results",e);
            return null;
        }
    }

private void export() {
    // simple CSV file export
    File csvFile = csvResults();
    if(csvFile==null) {
        Toast.makeText(this,"Could not create CSV file",Toast.LENGTH_LONG).show();
        return;
    }
    Intent shareIntent = new Intent();
    shareIntent.setAction(Intent.ACTION_SEND);
    shareIntent.setType("text/plain");
    shareIntent.putExtra(Intent.EXTRA_SUBJECT, systemInfo.deviceName);
    shareIntent.putExtra(Intent.EXTRA_TITLE, systemInfo.deviceName+".csv");
    // direct file URIs are not allowed on Android 24 or higher (FileUriExposedException)
    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Uri contentUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", csvFile);
        shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
    } else {
        // standard for older Android versions
        Uri uri = Uri.fromFile(csvFile);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
    }
    startActivity(shareIntent);
}


    private void prefs() {
        Intent i = new Intent(MainActivity.this, PrefsActivity.class);
        startActivity(i);
    }

    private void about() {
        new AboutFragment().show(getSupportFragmentManager(), "dialog");
    }

    private boolean hasSDCard() {
        for(StorageInfo storageInfo: storageInfos) {
            if(storageInfo.storageType != StorageInfo.StorageType.Flash && storageInfo.totalSize != 0)
                return true;
        }
        return false;
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_fulldiskwrite).setEnabled(hasSDCard());
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_export) {
            export();
            return true;
        } else if(id==R.id.action_fulldiskwrite) {
            fullwrite(storageInfos);
            return true;
        } else if(id==R.id.action_prefs) {
            prefs();
            return true;
        } else if(id==R.id.action_about) {
            about();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // context menu only for groups (storageInfos)
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v == storageListView) {
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
            int itemType = ExpandableListView.getPackedPositionType(info.packedPosition);
            int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                StorageInfo storageInfo = storageInfos[groupPosition];
                menu.setHeaderTitle(storageInfo.info(this).name);
                getMenuInflater().inflate(R.menu.menu_context_main, menu);
                menu.findItem(R.id.action_fulldiskwrite).setVisible(storageInfo.storageType != StorageInfo.StorageType.Flash);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_measure) {
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
            int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            StorageInfo storageInfo = storageInfos[groupPosition];
            measure(new StorageInfo[] { storageInfo});
            return true;
        } else if(id == R.id.action_fulldiskwrite) {
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
            int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            StorageInfo storageInfo = storageInfos[groupPosition];
            fullwrite(new StorageInfo[] { storageInfo});
        }
        return super.onContextItemSelected(item);
    }
}
