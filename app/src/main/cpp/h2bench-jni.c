/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <time.h>

#define NANOSEC_PER_SEC 1000000000LL
#define MIN(a,b) ((a)<(b)? (a) : (b))


static int64_t nanoTime() {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return (int64_t) now.tv_sec*NANOSEC_PER_SEC + now.tv_nsec;
}

JNIEXPORT jlong JNICALL
Java_com_linkesoft_h2bencha_BenchmarkNative_writeFileJNI( JNIEnv* env,
                                                    jobject thiz,jstring filepath, jlong bytesToWrite, jbyteArray jbytes)
{
    const char* path = (*env)->GetStringUTFChars(env, filepath, NULL);
    jbyte *bytes = (*env)->GetByteArrayElements(env,jbytes,NULL);
    long blocklength = (*env)->GetArrayLength(env,jbytes);

    int64_t startTime = nanoTime();
    size_t bytesWritten = 0;
    FILE* file = fopen(path,"w");
    if(file==NULL)
        return 0; // cannot write
    while(bytesWritten < bytesToWrite) {
        bytesWritten += fwrite(bytes,1,MIN(bytesToWrite,blocklength),file);
    }
    fclose(file);
    int64_t endTime = nanoTime();
    (*env)->ReleaseByteArrayElements(env,jbytes,bytes,JNI_ABORT);
    return (jlong)(endTime-startTime);
}

JNIEXPORT jlong JNICALL
Java_com_linkesoft_h2bencha_BenchmarkNative_readFileJNI( JNIEnv* env,
                                                    jobject thiz,jstring filepath,  jlong bytesToRead, jbyteArray jbytes)
{
    const char* path = (*env)->GetStringUTFChars(env, filepath, NULL);
    jbyte *bytes = (*env)->GetByteArrayElements(env,jbytes,NULL);
    long blocklength = (*env)->GetArrayLength(env,jbytes);

    int64_t startTime = nanoTime();
    size_t bytesRead = 0;
    FILE* file = fopen(path,"r");
    if(file==NULL)
        return 0; // cannot write
    while(bytesRead < bytesToRead) {
        bytesRead += fread(bytes,1,MIN(bytesToRead,blocklength),file);
    }
    fclose(file);
    int64_t endTime = nanoTime();
    (*env)->ReleaseByteArrayElements(env,jbytes,bytes,JNI_ABORT);
    return (jlong)(endTime-startTime);
}


